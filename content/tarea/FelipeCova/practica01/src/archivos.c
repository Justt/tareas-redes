#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "lista.h"

void insertar_archivo(struct lista*);
void leer_archivo(struct lista*);
void eliminar_archivo(struct lista*);
void imprimir_archivos(struct lista*);
int es_archivo(const char*);

int main()
{
  struct lista lista_archivos = { cabeza: 0, longitud:0 };

  while(1)
  {
    char *menu = "\n1. Insertar archivo\n2. Leer archivo\n3. Eliminar archivo\n4. Imprimir archivos";
    int opcion;
    printf("%s\n\nIngresar la opción deseada: ", menu);
    scanf("%d", &opcion);

    if(opcion == 1) { insertar_archivo(&lista_archivos); }
    else if(opcion == 2)  { leer_archivo(&lista_archivos); }
    else if(opcion == 3) { eliminar_archivo(&lista_archivos); }
    else if(opcion == 4) { imprimir_archivos(&lista_archivos); }
    else { printf("Opción incorrecta"); }
  }
  return 0;
}

void insertar_archivo(struct lista *lista)
{
  char *archivo = malloc(sizeof(char) * 255);
  printf("Ingresar el nombre del archivo: ");
  scanf("%s", archivo);
  agrega_elemento(lista, archivo);
}

void leer_archivo(struct lista *lista)
{
  if(lista->longitud < 1)
  {
    fprintf(stderr, "No hay archivos en la lista\n" );
    return;
  }

  int n;
  printf("Ingresar el índice del archivo: ");
  scanf("%d", &n);

  char *archivo = obten_elemento(lista, n);


  if(archivo == 0) {
    fprintf(stderr, "Índice fuera de rango.\n" );
    return;
  }

  if(!es_archivo(archivo))
  {
    DIR *d;
    struct dirent *dir;
    d = opendir(archivo);
    if(d)
    {
      while ((dir = readdir(d)) != NULL) { printf("%s\n", dir->d_name); }
      closedir(d);
    }
    else { fprintf(stderr, "Problemas al abrir el archivo. Verificar que existe.\n"); }
    return;
  }

  FILE *fp;

  if ((fp = fopen(archivo,"r")) == NULL) {
    fprintf(stderr, "Problemas al abrir el archivo. Verificar que existe.\n");
    return;
  }

  char line[256];
  while (fgets(line, sizeof(line), fp))
  {
    printf("%s", line);
  }

  fclose(fp);
}

void eliminar_archivo(struct lista *lista)
{
  if(lista->longitud < 1)
  {
    fprintf(stderr, "No hay archivos en la lista\n" );
    return;
  }

  int n;
  printf("Ingresar el índice del archivo: ");
  scanf("%d", &n);

  char *archivo = elimina_elemento(lista, n);

  if(archivo == 0) {
    fprintf(stderr, "Índice fuera de rango.\n" );
    return;
  }

  free(archivo);
}

void imprimir_archivos(struct lista *lista)
{
  if(lista->longitud < 1)
  {
    fprintf(stderr, "No hay archivos en la lista\n" );
    return;
  }

  int i;
  int tam = lista->longitud;
  printf("********** Archivos **********\n");
  for(i = 0; i < tam; i++)
  {
    char *archivo = obten_elemento(lista, i);
    printf("%d - %s\n", i, archivo);
  }
  printf("******************************\n");
}

int es_archivo(const char *ruta)
{
  struct stat stat_ruta;
  stat(ruta, &stat_ruta);
  return S_ISREG(stat_ruta.st_mode);
}
