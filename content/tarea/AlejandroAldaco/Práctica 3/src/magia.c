#include <stdio.h>

typedef enum _boolean {FALSE,TRUE} boolean; // Tipo boolean
boolean isType(char *file, char *id, int length); // Funcion para saber si es de un tipo

int main(int argv,char *argc[])
{
    if (argv>1)
    {
        //Números mágicos
        char gif[] = {0x47,0x49,0x46,0x38,0x37,0x61}; char gif2[] = {0x47,0x49,0x46,0x38,0x39,0x61};
        char elf[] = {0x7F,0x45,0x4C,0x46};
        char png[] = {0x89,0x50,0x4E,0x47,0x0D,0x0A,0x1A,0x0A};
        char zip[] = {0x50,0x4B,0x03,0x04}; char zip2[] = {0x50,0x4B,0x05,0x06}; char zip3[] = {0x50,0x4B,0x07,0x08};
        char pdf[] = {0x25,0x50,0x44,0x46,0x2d};
        char mp3[] = {0x49,0x44,0x33}; char mp32[] = {0xFF,0xFB};
        char exe[] = {0x4D,0x5A};
        
        //Recorre cada uno de los nombres de archivo que se pasaron como argumento y verifica si es de algún tipo
        for (int i = 1; i < argv; i++)
        {
            if (isType(argc[i], gif, 6) ||
                isType(argc[i], gif2, 6))
                printf ("%s: Archivo GIF\n",argc[i]);
            
            else if (isType(argc[i], elf, 4))
                printf ("%s: Archivo ELF\n",argc[i]);
            
            else if (isType(argc[i], png, 8))
                printf ("%s: Archivo PNG\n",argc[i]);
            
            else if (isType(argc[i], zip, 4) ||
                     isType(argc[i], zip2, 4) ||
                     isType(argc[i], zip2, 4))
                printf ("%s: Archivo ZIP\n",argc[i]);
            
            else if (isType(argc[i], pdf, 5))
                printf ("%s: Archivo PDF\n",argc[i]);
            
            else if (isType(argc[i], mp3, 3) ||
                     isType(argc[i], mp32,2))
                printf ("%s: Archivo MP3\n",argc[i]);
            
            else if (isType(argc[i], exe, 2))
                printf ("%s: Archivo EXE\n",argc[i]);
            
            else
                printf("%s: es desconocido\n",argc[i]);
        }
    }
    return 0;
}


/* @param file nombre de archivo
 * @param id número mágico
 * @param length tamaño del número mágico
 * return TRUE si coinciden o FALSE en otro caso
 */
boolean isType(char *file, char *id, int length)
{
    char magics[length]; // array que contendra los numero magicos
    FILE *fich = fopen(file,"rb"); // Abrimos el archivo
    fread(magics, 1, length, fich); // Lemos los primeros 3 bytes
        for (int i = 0; i < length; i++) // Recorremos el array
    {
        if(magics[i] != id[i]) // Si no son iguales
        {
            return FALSE; // Cambiamos el valor de retorno
        }
    }
    fclose(fich); // Cerramos el archivo
    return TRUE; // Regresamos el valor
}
