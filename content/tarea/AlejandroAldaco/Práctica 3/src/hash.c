#include <stdio.h>
#include "md5.h"
#include "sha1.h"
#include "sha256.h"

typedef enum _boolean {FALSE,TRUE} boolean; // Tipo boolean
boolean isType(char *file, char *id, int length); // Funcion para saber si es de un tipo

int main(int argv, char *argc[])
{
    if (argv > 2)
    {
        char md5sum[] = "md5";
        char sha1sum[] = "sha1";
        char sha256sum[] = "sha256";
        char calc_hash[65];
        for (int i = 2; i < argv; i++)
        {
            FILE *fich = fopen(argc[i], "r");
            if (isType(argc[1], md5sum, 3))
            {
                if (fich)
                    md5_file(argc[i],calc_hash);
                else
                    md5(argc[i],calc_hash);
            }
            else if (isType(argc[1], sha1sum, 4))
            {
                if (fich)
                    sha_file(argc[i],calc_hash);
                else
                    sha(argc[i],calc_hash);
            }
            else if (isType(argc[1], sha256sum, 6))
            {
                if (fich)
                    sha256_file(argc[i],calc_hash);
                else
                    sha256(argc[i],calc_hash);
            }
            printf("%s ",calc_hash);
            if (!fich)
                printf("Hash de la cadena: ");
            else
                printf(" --> ");
            printf("\"%s\"\n",argc[i]);
        }
    }
    return 0;
}

/* @param tipo de cálculo
 * @param length tamaño de la cadena del tipo de cálculo
 * return TRUE si coinciden o FALSE en otro caso
 */
boolean isType(char *arg, char *id, int length)
{
    for (int i = 0; i < length; i++) // Recorremos el array
    {
        if(arg[i] != id[i]) // Si no son iguales
        {
            return FALSE; // Cambiamos el valor de retorno
        }
    }
    return TRUE; // Regresamos el valor
}
