#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
/*el codigo de conexion, asi como la funcion trim fueron obtenidas de tutoriales de internet*/
/*https://es.wikibooks.org/wiki/Programaci%C3%B3n_en_C/Sockets*/
/*http://www.martinbroadhurst.com/trim-a-string-in-c.html*/

char *ltrim(char *str, const char *seps)
{
    size_t totrim;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    totrim = strspn(str, seps);
    if (totrim > 0) {
        size_t len = strlen(str);
        if (totrim == len) {
            str[0] = '\0';
        }
        else {
            memmove(str, str + totrim, len + 1 - totrim);
        }
    }
    return str;
}

char *rtrim(char *str, const char *seps)
{
    int i;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    i = strlen(str) - 1;
    while (i >= 0 && strchr(seps, str[i]) != NULL) {
        str[i] = '\0';
        i--;
    }
    return str;
}

char *trim(char *str, const char *seps)
{
    return ltrim(rtrim(str, seps), seps);
}


int main(int argc, char **argv){
  if(argc<2)
  {
    printf("<host> <puerto>\n");
    return 1;
  }
  struct sockaddr_in cliente;
  struct hostent *servidor;
  servidor = gethostbyname(argv[1]);
  if(servidor == NULL)
  { 
    printf("Host erróneo\n");
    return 1;
  }
  int puerto, conexion;
  char param[100];
  conexion = socket(AF_INET, SOCK_STREAM, 0); 
  puerto=(atoi(argv[2])); 
  bzero((char *)&cliente, sizeof((char *)&cliente)); 
  cliente.sin_family = AF_INET;
  cliente.sin_port = htons(puerto);
  bcopy((char *)servidor->h_addr, (char *)&cliente.sin_addr.s_addr, sizeof(servidor->h_length));
  if(connect(conexion,(struct sockaddr *)&cliente, sizeof(cliente)) < 0)
  {
    printf("Error conectando con el host\n");
    close(conexion);
    return 1;
  }
  printf("Conectado con %s:%d\n",inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));
  printf("escibe 'salir' para terminar la conexion");
  printf("Comandos:\n");
  int bandera = 1;
  while(bandera){
    fgets(param, 100, stdin);
    char * comando;
    if(strcmp(param,"salir") == 10){
      bandera = 0;
      comando = param;
    } else {
      comando = strcat(trim(param,"\t\v\f\r\n"), " >> salida.txt");
    }
    
    send(conexion, comando, 100, 0);
    bzero(param, 100);
    recv(conexion, param, 100, 0);
    printf("%s", param);
  }
return 0;
}
