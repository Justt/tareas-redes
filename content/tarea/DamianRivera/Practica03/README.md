## Práctica3
Damián Rivera González.

(El contenido de la carpeta ejemplos, son archivos para probar el ejercicio 1 - file - ) 

Luego de encontrarse dentro de la carpte Practica03, se deberán compilar los programas
mediante el comando 'make' , el cual generará tres archivo binarios:

    file, hash, strings

###File
El archivo 'file' será el ejecutable para el primer ejercicio que dado un archivo pasado como parametro se inidicará si este es de tipo GIF, ELF, PNG, ZIP, PDF, MP3 y EXE.

Para su ejecución simplemente ingresar en terminal

    ./file <archivo> ...

Ejemplo
![](imagenes/file.png)

###Strings
El archivo 'strings' será el ejecutable para imprimir los caracteres válidos (con un rango de byte entre 32 y 126), se le debe pasar un archivo con contenido binario. Se deja como archivo de prueba el archivo "archivo"

Para su ejecución sería el siguiente uso:

    ./strings <archivo>

Ejemplo
![](imagenes/strings.png)

###Hash
El archivo 'hash' será el ejecutable para imprimir el valor Md5 o Sha1 o Sha256 de los archivos que se pasan como parametros. Consta del tipo de función hash Md5 o Sh1 o Sh256.

Para su uso sería la siguiente estructura:

    ./hash <opcion> <archivo1> <archivo2> ... <archivoN>
    donde <opcion> = md5 o sha1 o sha256

Ejemplo
![](imagenes/hash.png)
