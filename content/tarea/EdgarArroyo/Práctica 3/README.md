## 1. Magia
![](imgs/magia.png)

## 2. Strings
![](imgs/strings.png)

## 3. Hashes
![](imgs/hash.png)

## 4. Filtros
TODO

## 5. Extra
- Direcciones IP: `172.16.16.1` y `172.16.16.217`
- Direcciones MAC de equipos: `00:50:56:c0:00:08` y `00:0c:29:79:af:e0`
- MD5 de archivo: `6fe658c629bcb4327d6c475e06d899fc`
- Usuario: `david`
- Contraseña: `blackstar`
De la captura leí las líneas de texto con el programa `strings`, con lo que se mostraban mensajes de una conversación. Ahí aparecía la contraseña para un archivo junto con su valor MD5. Luego aparecía una solicitud HTTP con dirección /secreto. En Wireshark extraje el contenido del archivo, quité el encabezado de HTTP y obtuve un archivo ZIP (cuyo MD5 es el ya mencionado), que pude extraer con la contraseña `hola123`. Solo contenía un archivo y este parece contener bytes de relleno además de la cadena que contiene un user y un password, que también pude ver con `strings`.
![](imgs/captura.png)
![](imgs/secreto.png)