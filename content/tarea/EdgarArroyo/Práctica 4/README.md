Por falta de recursos tuve que usar VirtualBox, pero todo salió bien.
Solo que usé una interfaz con dirección 192.168.51.0. (Al principio solo estaba
probando para ver si habría problemas, pero todo fue saliendo bien y decidí
dejarlo así; en VirtualBox solo hay que agregar una nueva interfaz virtual con
la IP que uno quiera.)

## Servidor DHCP
Captura de pantalla de Wireshark con los paquetes de DHCP cuando se le asignó
dirección a la nueva máquina.
![](imgs/paquetes_dhcp.png)

En la nueva máquina vemos su dirección IP
![](imgs/direccion_dhcp.png)

Y el registro del servidor
![](imgs/log_dhcp.png)

## PXE
Siguiendo las instrucciones para configurar tanto DHCP como TFTP, quedó listo
el servicio de PXE para arrancar desde la imagen de Debian. Se anexa la captura
de tráfico que corresponde al momento en que se arrancó la nueva máquina. En la
siguiente imagen se puede ver el arranque.
![](imgs/instala_PXE.gif)

## Preguntas
#### ¿Cómo funciona el protocolo DHCP?
Una sesión regular de DHCP consiste de cuatro etapas:
1. Discovery. El cliente hace un broadcast con la petición. Es broadcast porque
en principio el cliente no sabe la dirección del servidor.

2. Offer. Cuando el servidor recibe la petición del cliente, reserva una
dirección IP y le manda un mensaje al cliente que incluye esta dirección IP
junto con otra información. Para hacer llegar la información usa la dirección
MAC del cliente.

3. Request. En este paso el cliente, luego de que recibió el ofrecimiento de una
dirección IP (posiblemente varias direcciones ofrecidas por varios servidores),
confirma la dirección IP que escogió y le avisa al servidor que se la ofreció.
De nuevo hace un broadcast, pero en los campos del mensaje va incluida la
dirección IP del servidor al que se contesta, además de la dirección IP que fue
escogida. Otros servidores DHCP que reciben este mensaje, al ver que el mensaje
es para otro servidor, simplemente descartan el ofrecimiento.

4. Acknowledge. Finalmente, el servidor confirma que se hizo la asignación de la
dirección IP. Además en el mensaje enviado se incluyen otros parámetros, como
direcciones de servidores DNS o dirección del router. Después de esto se espera
que el cliente haya establecido los parámetros que recibió en el mensaje del
servidor.

#### ¿Cómo se filtra el tráfico del protocolo DHCP en una captura de tráfico en Wireshark y en tcpdump?

Haciendo que se revise el tráfico que pasa por los puertos 67 o 68 (usados por
el servidor y los clientes, respectivamente), por ejemplo con tcpdump:
```sh
$ tcpdump -i <network-interface> port 67 or port 68 -e -n
```
mientras que para Wireshark podemos usar
```
udp.port eq 67 or udp.port eq 68
```

#### ¿Qué es el estándar PXE y cuál es su utilidad?
Es un protocolo que sirve para instalar un sistema operativo a través de la red,
en vez del uso de medios de almacenamiento (como memorias flash, CD o discos
duros). PXE usa varios protocolos (UDP, IP, DHCP y TFTP) para realizar la
configuración y comunicación de los datos necesarios para la instalación del SO.
Se usa una arquitectura cliente-servidor: el servidor tiene los archivos para la
instalación, y un cliente es un dispositivo que al arrancar se intenta conectar
al servidor para ejecutar la instalación del SO.

