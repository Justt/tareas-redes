## Práctica 2

### Compilación
$ make all

### Ejecución
$ ./servidor.o

$ ./cliente.o

### Imágenes

Servidor corriendo
![alt text](img/1.png)

Cliente corriendo
![alt text](img/2.png)

Cliente haciendo peticiones
![alt text](img/3.png)
