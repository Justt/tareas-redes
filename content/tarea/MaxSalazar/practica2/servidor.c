#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

int main(int argc, char *argv[]){
  int sockfd, newsockfd, port;
  socklen_t clilen;
  char input[512] = {0};
  char output[512] = {0};
  char command[20] = {0};
  struct sockaddr_in serv_addr, cli_addr;
  char str[INET_ADDRSTRLEN];
  FILE *fp;
  if (argc < 2) {
    fprintf(stderr, "Falta el puerto\n");
    return 1;
   }
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
    fprintf(stderr, "Error al crear el socket\n");
    return 1;
  }
  printf("Socket creado correctamente\n");
  bzero((char *) &serv_addr, sizeof(serv_addr));
  port = atoi(argv[1]);
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    fprintf(stderr, "Error al enlazar\n");
    return 1;
  }
  printf("Ligado correctamente\n");
  if (listen(sockfd, 3) < 0){
    fprintf(stderr, "Error al escuchar\n");
    return 1;
  }
  printf("Escuchando, esperando conexiones entrantes\n");
  clilen = sizeof(cli_addr);
  newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  inet_ntop(AF_INET,&(cli_addr.sin_addr),str,INET_ADDRSTRLEN);
  printf("server: Conexión aceptada desde %s:%d\n", str, port);
  for(;;){
    bzero(input,sizeof(input));
		bzero(output,sizeof(output));
		bzero(command,sizeof(command));
		read(newsockfd, input, sizeof(input));
		if(strcmp(input, "EOF") == 0)
      break;
		printf("Comando a ejecutar: %s\n", input);
		fp = popen(input,"r");
    while(fgets(command, sizeof(command), fp) != NULL)
      strcat(output,command);
		pclose(fp);
		write(newsockfd,output,strlen(output));
	}
	close(newsockfd);
	printf("Se ha cerrado la conexión\n");
  return 0;
}
