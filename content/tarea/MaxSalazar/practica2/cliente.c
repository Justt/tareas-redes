#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]){
	int sockfd, port, pton_ret;
  struct sockaddr_in serv_addr;
	char input[512] = {0};
	char command[512] = {0};
	if(argc < 3){
    fprintf(stderr, "Falta el la IP del servidor o el puerto.\n");
    return 1;
  }
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
    fprintf(stderr, "Error al crear el socket\n");
    return 1;
  }
  printf("Socket creado correctamente\n");
  port = atoi(argv[2]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);
	pton_ret = inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);
  switch (pton_ret) {
    case -1:
      fprintf(stderr, "No se pudo convertir la IP\n");
      return 1;
    case 0:
      fprintf(stderr, "IP inválida\n");
      return 1;
  }
	if(connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
		fprintf(stderr, "Falló la conexión con el servidor\n");
	printf("Para terminar la conexión escribe 'EOF'\n");
  for(;;){
    bzero(input, sizeof(input));
    bzero(command, sizeof(command));
    if (strcmp(command, "EOF") == 0)
      break;
		fgets(command, sizeof(command) - 1, stdin);
		if( command[strlen(command) - 1] == '\n')
    		command[strlen(command) - 1] = 0;
		write(sockfd, command, strlen(command));
		read(sockfd, input, sizeof(input) - 1);
		printf("%s",input);
	}
	printf("Se ha terminado la conexión\n");
  return 0;
}
