#include "lista.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void inserta(struct lista *l){
  printf("Pon el nombre del archivo.\n");
  char fname[20] = "";
  scanf("%20s", fname);
  fflush(stdin);
  if( access(fname, F_OK) != -1 ){
    char *p = (char *)malloc(sizeof(char) * strlen(fname));
    strcpy(p, fname);
    agrega_elemento(l, p);
    printf("agregado correctamente el archivos %s\n", fname);
  }else
    fprintf(stderr, "no existe el archivo %s\n", fname);
}

void lee(struct lista *l){
  int input;
  printf("leeré el archivo en la posición que pongas de mi lista.\n");
  scanf("%d", &input);
  fflush(stdin);
  if(l -> longitud < input){
    fprintf(stderr, "sólo tengo %d archivos\n", l -> longitud);
  } else{
    for(int i = 0; i < l -> longitud; i++){
      if(i == input){
        char *fname = (char *)obten_elemento(l, input);
        FILE *fptr = fopen(fname, "r");
        if(fptr == NULL){
          fprintf(stderr, "no pude abrir el archivo %s\n", fname);
          return;
        }
        char c = fgetc(fptr);
        while(c != EOF){
          printf("%c", c);
          c = fgetc(fptr);
        }
        fclose(fptr);
      }
    }
  }
}

void elimina(struct lista *l){
  int input;
  printf("Inserta la posición del archivo en mi lista.\n");
  scanf("%d", &input);
  fflush(stdin);
  if(l -> longitud < input){
    fprintf(stderr, "Sólo tengo %d archivos.\n", l -> longitud);
  } else{
    elimina_elemento(l, input);
    printf("Elemento eliminado correctamente\n");
  }
}

void imprime(struct lista *l){
  struct nodo *aux = l -> cabeza;
  for(int i = 0; i < l -> longitud; i++){
    printf("%d. %s\n", i, (char *)(aux -> elemento));
    aux = aux -> siguiente;
  }
}


int main(){
  struct lista l = {cabeza : 0, longitud : 0};
  int input;
  for(;;){
    printf("1. Insertar un archivo\n");
    printf("2. Leer un archivo\n");
    printf("3. Eliminar un archivo\n");
    printf("4. Imprimir un archivo\n");
    printf("5. Salir\n");
    scanf("%d", &input);
    fflush(stdin);
    switch(input){
      case 1:
        inserta(&l);
        break;
      case 2:
        lee(&l);
        break;
      case 3:
        elimina(&l);
        break;
      case 4:
        imprime(&l);
        break;
      case 5:
        return 0;
      default:
        printf("Pon un valor válido.\n");
    }
  }
  return 0;
}
