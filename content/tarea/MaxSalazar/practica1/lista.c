#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

void agrega_elemento(struct lista * lista, void * elemento){
  struct nodo *newNodo, *aux;
  newNodo = (struct nodo*) malloc(sizeof(struct nodo));
  newNodo -> elemento = elemento;
  newNodo -> siguiente = NULL;
  if(lista -> longitud == 0)
    lista -> cabeza = newNodo;
  else{
    aux = lista -> cabeza;
    while(aux -> siguiente != NULL)
      aux = aux -> siguiente;
    aux -> siguiente = newNodo;
  }
  lista -> longitud++;
}

void * obten_elemento(struct lista * lista, int n){
  struct nodo *aux = lista -> cabeza;
  void * elemento;
  for(int i = 0; i < lista -> longitud; i++){
    if(i == n){
      elemento = aux -> elemento;
      break;
    }
    aux = aux -> siguiente;
  }
  return elemento;
}

void * elimina_elemento(struct lista * lista, int n){
  struct nodo *aux, *u;
  void * elemento;
  if (lista -> longitud < n)
    return elemento;
  aux = lista -> cabeza;
  if(n == 0){
    elemento = aux -> elemento;
    lista -> cabeza = aux -> siguiente;
    lista -> longitud--;
    free(aux);
    return elemento;
  }

  for(int i = 0; i < lista -> longitud; i++){
    if(i == (n - 1)){
      u = aux -> siguiente;
      elemento = u -> elemento;
      aux -> siguiente = u -> siguiente;
      free(u);
      lista -> longitud--;
      break;
    }
    aux = aux -> siguiente;
  }
  return elemento;
}

void aplica_funcion(struct lista * lista, void (*f)(void *)){
  struct nodo *aux = lista -> cabeza;
  for(int i = 0; i < lista -> longitud; i++){
    f(aux -> elemento);
    aux = aux -> siguiente;
  }
}
