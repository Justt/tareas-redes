#include <stdio.h>

int strings(char filename[]){

  FILE *f;

  if( (f = fopen(filename, "rb")) == NULL){
    return 1; //No se puede abrir
  }

  int c;
  while ((c = fgetc(f)) != EOF){
    if ((32 <= c) && (c <= 126))
      printf("%c", c);
  }

  return 0;
}

int main(int argc, char *argv[]){

  if(argc != 2){
    return 1; // un archivo solamente
  }

  return strings(argv[1]);

}
